from django.db import models
from django.contrib.auth.models import User

class Visitor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.URLField(blank=False)
    birth_date = models.DateField(blank=True)
