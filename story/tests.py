from django.test import TestCase, Client
from django.urls import resolve

from story.views import index, accountSignout
import time

class StoryTest(TestCase):

    def test_index_page_response_success(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_page_use_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_page_use_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_signup_page(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_login_page(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signout_page(self):
        response = Client().get('/logout/', follow=True)
        self.assertTemplateUsed(response, 'index.html')

    def test_signup_valid(self):
        form_data = {
            'name': "The Hobbit",
            'email': "test@test.com",
            'username': "usertest",
            'password1': "SomeRandom123",
            'password2': "SomeRandom123",
            'photo': "https://cdn.cnn.com/cnnnext/dam/assets/200424060716-nasa-worm-logo-super-169.jpg",
            'birth_date': "2000-01-01",
        }
        request = self.client.post('/signup/', data = form_data)
        self.assertEqual(request.status_code, 302)

    def test_signup_invalid(self):
        form_data = {
            'name': "The Hobbit",
            'email': "test@test.com",
            'username': "a",
            'password1': "a",
            'password2': "a"
        }
        request = self.client.post('/signup/', data = form_data)
        self.assertEqual(request.status_code, 200)

    def test_login_valid(self):
        form_data = {
            'name': "The Hobbit",
            'email': "test1@test.com",
            'username': "usertest1",
            'password1': "SomeRandom123",
            'password2': "SomeRandom123",
            'photo': "https://cdn.cnn.com/cnnnext/dam/assets/200424060716-nasa-worm-logo-super-169.jpg",
            'birth_date': "2000-01-01",
        }
        request = self.client.post('/signup/', data = form_data)
        form_data = {
            'username': "usertest1",
            'password': "SomeRandom123",
        }
        request = self.client.post('/login/', data = form_data, follow=True)
        self.assertEqual(request.status_code, 200)
        html_response = request.content.decode('utf8')
        self.assertIn(
            "https://cdn.cnn.com/cnnnext/dam/assets/200424060716-nasa-worm-logo-super-169.jpg",
            html_response
        )
        self.client.get('/logout/')


    def test_login_invalid(self):
        form_data = {
            'name': "The Hobbit",
            'email': "test2@test.com",
            'username': "usertest2",
            'password1': "SomeRandom123",
            'password2' : "SomeRandom123",
            'photo': "https://cdn.cnn.com/cnnnext/dam/assets/200424060716-nasa-worm-logo-super-169.jpg",
            'birth_date': "2000-01-01",
        }
        self.client.post('/signup/', data = form_data)
        form_data = {
            'username': "a",
            'password': "a",
        }
        request = self.client.post('/login/', data = form_data)
        self.assertEqual(request.status_code, 200)
